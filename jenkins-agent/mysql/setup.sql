-- create kisphp database and user
CREATE DATABASE IF NOT EXISTS default;

CREATE USER 'default_user'@'%' IDENTIFIED BY 'default_pass';
GRANT ALL PRIVILEGES ON *.* TO 'default'@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;
