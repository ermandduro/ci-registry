#!/usr/bin/env bash

LOG_FILE='/dev/stdout'

echo "Start mysql server (MariaDB 10)" | tee -a $LOG_FILE
/etc/init.d/mysql start

# wait until mysql is up and running
COUNTER=1
while ! mysqladmin ping -uroot --silent;
do
    echo "Try to connect to mysql ${COUNTER}" | tee -a $LOG_FILE
    COUNTER=`expr $COUNTER + 1`
    if [[ $COUNTER -gt 30 ]]; then
        echo "Could not connect to MySQL" | tee -a $LOG_FILE
        exit 1
        break
    fi
    sleep 2;
done

echo "Create default database" | tee -a $LOG_FILE
mysql -uroot < /setup.sql

echo "Start php-fpm" | tee -a $LOG_FILE
/etc/init.d/php-fpm restart

echo "Start nginx" | tee -a $LOG_FILE
/etc/init.d/nginx restart
