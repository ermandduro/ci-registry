# Create debian packages

In any GNU/Linux distribution, which have a package manager on board, is recommended to install software by using "packages".

At some point it may happen that create a package will be easier than build it from source again and again. For example: some programs required for several colleagues and not all of them is able to understand why building from source code failed.

If you want to create a package that can/will be included in the official Debian repository you must to get acquainted with the following materials :

- [the Debian New Maintainers' Guide](http://www.debian.org/doc/manuals/maint-guide/index.en.html)
- [Debian Packaging Tutorial](http://www.debian.org/doc/devel-manuals#packaging-tutorial)
- [Debian Policy](http://www.debian.org/doc/debian-policy/)
- [Developers Reference](http://www.debian.org/doc/manuals/developers-reference/)

## Building from source

### Step 1. GPG key
The first thing you need is generate a gpg key. Package will be signed by this key. Run this commands in your terminal:

```bash
gpg --gen-key
gpg -a --output ~/.gnupg/YOUR_NAME.gpg --export 'YOUR NAME'
gpg --import ~/.gnupg/YOUR_NAME.gpg
```
It's important to remember that in next stage it will be necessary to use the same name and email that were used for creating the key.

### Step 2. Prepare the environment
You need install these packages for further building:
```bash
sudo apt-get install build-essential autoconf automake \
autotools-dev dh-make debhelper devscripts fakeroot \
xutils lintian pbuilder
```
### Step 3. Obtain & prepare source code
Download tarball with source code. Unpack it and rename directory like this: "name-version". Important: the directory name must be in lowercase.

On the same level with that directory you need to place the archive with the source code. You can simply archive a directory that you just created.
```bash
mkdir -p ~/build/memcached/1.4.17
cd ~/build/memcached/1.4.17
wget -c http://www.memcached.org/files/memcached-1.4.17.tar.gz
tar -xzf memcached-1.4.17
ls

... memcached-1.4.17/ memcached-1.4.17.tar.gz
```

### Step 4. Debianization
Now we will prepare package structure.
```bash
cd ~ / build/memcached/1.4.17/memcached-1.4.17
dh_make -e youremail@address -f .. / memcached-1.4.17.tar.gz
```

Do not forget that you must use the same email, that was used to generate the key . After you run this command in the terminal you'll see the following prompt :
```bash
Type of package: single binary, indep binary, multiple binary,
    library, kernel module, kernel patch?
[s /i/m/l/k/n/b]
```
Choose the easiest option - s

In our directory appeared a new subdirectory called "debian" which contains the files necessary for further assembly. Now we must edit the information about our package.

"control" file
First of all we must find out dependencies for our package:
```bash
dpkg-depcheck -d ./configure
```
In terminal we will see something like that:
```bash
Packages needed:
    mime-support
    libsigsegv2:amd64
    gawk
    libevent-dev
```

After that we must edit debian/control file like this:

```bash
Source: memcached
Section: web
Priority: optional
Maintainer: YOUR NAME <youremail.org>
Build-Depends: debhelper (> = 8.0.0), autotools-dev,
  mime-support, libsigsegv2, gawk, libevent-dev
Standards-Version: 3.9.4
Homepage: http://memcached.org/

Package: memcached
Architecture: any
Depends: $ {shlibs: Depends}, $ {misc: Depends}
Description: High-performance, distributed memory object caching system
  Memcached is an in-memory key-value store for small chunks of arbitrary data (strings, objects)
```

### "copyright" file
In the same "debian" folder, you will also find a "copyright" file which you need to edit. The important things to add to this file are the place you got the package from and the actual copyright notice and license.

### "changelog" file
Make sure that email used in this file is correct (it must be same as used for gpg-key generation)

### Step 5. Build a package
Ok. It's time to build our package. Let's do it:
```bash
dpkg-buildpackage -rfakeroot
```

If there had been no error in the previous stages the assembly process will receive a password prompt from the gpg-key. After that our new package will appear in directory one level up. You can install it by following command:
```bash
sudo dpkg -i memcached_1.4.17-1_amd64.deb
```
As you can see the assembly was carried out on the platform x86_64. Do you need another platform? No problem.

Assembly for a another platform
To build, for example, for the i386 platform we can use pbuilder - an automatic Debian Package Building system for personal development workstation environments.
```bash
cd ~/build/memcached/1.4.17
sudo pbuilder --create --architecture i386
sudo pbuilder --update
# memcached_1.4.17-1.dsc was generated when we had built a package
sudo pbuilder --build memcached_1.4.17-1.dsc
```
Package for i386 platform will be generated in the /var/cache/pbuilder/result directory
