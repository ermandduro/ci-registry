# Container packages:

- php 7.1
- composer
- nodejs
- npm
- yarn
- gulp

# How to load mysql container as service

```yaml
test_name:
  stage: stage_name
  services:
    - alias: "mysql"
      name: "registry.gitlab.com/ermandduro/ci-registry/mysql5.7:latest"
  script:
    - your/script/here
```

# How to use this repository

Update `Dockerfile` and then execute the following command to build the container

```bash
make build
```

If you want to test the container before pushing on gitlab, run:

```bash
make run
```

After everything is functional, update version number in `Makefile` and then execute:

```bash
make prepare
```

To push the new image on gitlab registry repository execute:

```bash
make push
```

# How to use the Container Registry

First log in to GitLab’s Container Registry using your GitLab username and password. If you have 2FA enabled you need to use a personal access token:
```bash
docker login registry.gitlab.com
```

Once you log in, you’re free to create and upload a container image using the common build and push commands
```bash
docker build -t registry.gitlab.com/ermandduro/ci-registry .
docker push registry.gitlab.com/ermandduro/ci-registry
```

## Use different image names

GitLab supports up to 3 levels of image names. The following examples of images are valid for your project:

```bash
registry.gitlab.com/ermandduro/ci-registry:tag
registry.gitlab.com/ermandduro/ci-registry/optional-image-name:tag
registry.gitlab.com/ermandduro/ci-registry/optional-name/optional-image-name:tag
```
